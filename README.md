## Basic steps to Install Private Docker Registry

```bash
#create its directory and cd to it
mkdir docker-registry
cd docker-registry
mkdir data
#I choose to authenticated Private Docker Registry with htpasswd install apache2-utils package to use htpasswd
sudo apt-get install apache2-utils -y
#create folder and cd to auth
mkdir auth
cd auth
#create user with bcrypt algorithm (note: make sure you're in directory auth before run the command line below).dalin here is a user
htpasswd -Bc registry.password dalin
#cd to back to docker registry
cd ../
#create docker compose file 
sudo touch docker-compose.yml
#paste the file from the second link below to it and save
#run docker compose
docker compose up -d
```
### add user to existing group
```bash
usermod -aG group_name user_name
```

### login 
```bash
docker login -u dalin -p 1111 image.dalin.host
docker tag image_name image.dalin.host/image_name
docker push image.dalin.host/image_name
#url
https://registry.dalin.host:8443/home
```
### docker registry path url
```bash
#no matter how you build/push/pull the image in registry, like registry_dn/image:tag
#in web access of registry, in repository of that image it will display
docker pull registry:5000/image:tag
#where is this "registry:5000" come from? =>
environment:
      ENV_DOCKER_REGISTRY_HOST: 
      ENV_DOCKER_REGISTRY_PORT:
#if you want to display registry_dn/image:tag in web access like this
docker pull registry_dn/image:tag
#make sure you run registry in port 443 (https)
```

### abit about nginx
```bash
sudo vi /etc/nginx/nginx.conf
### add that script below to http, save and exit
client_max_body_size 900m;
nginx -t
service nginx restart
```
### Reference

[Set up docker registry by digital ocean](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-20-04)

[docker registry docker compose yaml file](https://docs.google.com/document/d/1JzlMzKjq8UkBvomfOWzvvLPJwgazUq5uXTdZIu7PRVo/edit)